package com.jcgg.Entity;

// Parece ser que esta clase entidad representa lo que en rails seria el esquema del objeto
public class Student {
    
    // los atributos y tipos del objeto
    private int id;
    private String name;
    private String course;
    
    // constructor vacio del objeto
    public Student() {}
    
    // sobrecarga del constructor, con todos los atributos
    public Student(int id, String name, String course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }
    
    // getters y setters para los atributos del objeto estudiante
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getCourse() {
        return course;
    }
    
    public void setCourse(String course) {
        this.course = course;
    }
}
