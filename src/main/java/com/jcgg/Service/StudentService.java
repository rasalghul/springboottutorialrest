package com.jcgg.Service;

import com.jcgg.Dao.StudentDao;
import com.jcgg.Entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.Collection;

// anotacion que indica a spring que es un servicio
@Service
public class StudentService {
    
    // anotacion que indica a spring (autoconecta? los metodos...)
    @Autowired
    // aqui se puede cambiar entre los datos de mongo y los datos fake, mediante la anotacion de calificador
    @Qualifier("fakeData")
    private StudentDao studentDao;
    
    // el servicio studentservice puede invocar al metodo getallstudents que retorna un objeto coleccion con todos los estudiantes
    public Collection<Student> getAllStudents(){
        return studentDao.getAllStudents();
    }
    
    public Student getStudentById(int id){
        return this.studentDao.getStudentById(id);
    }
    
    public void removeStudentById(int id) {
        this.studentDao.removeStudentById(id);
    }
    
    public void updateStudent(Student student){
        this.studentDao.updateStudent(student);
    }
    
    public void createStudent(Student student) {
        this.studentDao.createStudent(student);
    }
}
