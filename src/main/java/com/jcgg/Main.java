package com.jcgg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

// Esta anotacion es para indicar que es una aplicacion de spring boot, hace el import de las clases arriba
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        // Punto de entrada de la aplicacion
        SpringApplication.run(Main.class, args);
    }
}
