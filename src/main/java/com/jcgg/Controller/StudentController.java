package com.jcgg.Controller;

import com.jcgg.Entity.Student;
import com.jcgg.Service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

// esta anotacion indica a spring que es un controler tipo REST
@RestController
// parece ser que esta anotacion es un tipo de route para que opere este controlador
@RequestMapping("/students")
public class StudentController {
    
    // esta anotacion indica a spring a (autoconectar? los objetos instanciados...)
    @Autowired
    private StudentService studentService;
    
    // parece que esta anotacion tipo ruta, es especifica a un tipo de ruta GET, que responde una coleccion de estudiantes, es invocada por el metodo getallstudents de la instancia que fue autoconectada arriba
    @RequestMapping(method = RequestMethod.GET)
    public Collection<Student> getAllStudents(){
        return studentService.getAllStudents();
    }
    
    // Esta ruta es para solicitar un estudiante solamente, se debe indicar el id del estudiante
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    // Este metodo regresa el estudiante
    public Student getStudentById(@PathVariable("id") int id){
        return studentService.getStudentById(id);
    }
    
    // ruta para eliminar un estudiante, ojo con la anotacion deletemapping
    @DeleteMapping(value = "/{id}")
    public void deleteStudentById(@PathVariable("id") int id){
        studentService.removeStudentById(id);
        System.out.println("Successfully deleted student with id: " + id);
    }
    
    // metodo para actualizar el registro de un estudiante, en el requestmapping le indicamos que consume un objeto que viene en formato JSON en el cuerpo de la peticion
    @RequestMapping(method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void updateStudent(@RequestBody Student student){
        studentService.updateStudent(student);
    }
    
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public void createStudent(@RequestBody Student student){
        studentService.createStudent(student);
    }
}
