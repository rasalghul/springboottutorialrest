package com.jcgg.Dao;

import com.jcgg.Entity.Student;

import java.util.Collection;

public interface StudentDao {
    // Metodo getter que obtendra todos los estudiantes y los guarda en un objeto coleccion
    Collection<Student> getAllStudents();
    
    // metodo getter que obtendra solamente un estudiante mediante el id que recibe de la ruta
    Student getStudentById(int id);
    
    // metodo setter para eliminar a un estudiante mediante el id que recibe como parametro en la peticion
    void removeStudentById(int id);
    
    // metodo para actualizar el registro de un estudiante
    void updateStudent(Student student);
    
    // metodo para crear un nuevo usuario en la base de datos
    void createStudent(Student student);
}
