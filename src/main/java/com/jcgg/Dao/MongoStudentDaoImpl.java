package com.jcgg.Dao;

import com.jcgg.Entity.Student;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

@Repository
@Qualifier("mongoData")
public class MongoStudentDaoImpl implements StudentDao {
    // aqui se hace la conexion a la BD Mongo
    
    @Override
    public Collection<Student> getAllStudents() {
        return new ArrayList<Student>(){
            {
                add(new Student(1, "Mario", "nothing"));
            }
        };
    }
    
    @Override
    public Student getStudentById(int id) {
        return null;
    }
    
    @Override
    public void removeStudentById(int id) {
    
    }
    
    @Override
    public void updateStudent(Student student) {
    
    }
    
    @Override
    public void createStudent(Student student) {
    
    }
}
